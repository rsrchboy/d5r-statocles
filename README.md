# Statocles in a box!

This is an image that can be used to build your
[Statocles](https://metacpan.org/release/Statocles) site with.

Note that you're probably going to want to set/change the timezone for this to
work effectively.

For an example of using this image with GitLab CI/CD:

```yaml
pages:
  stage: deploy
  image: registry.gitlab.com/rsrchboy/d5r-statocles:latest
  script:
    - ln -snf /usr/share/zoneinfo/America/Chicago /etc/localtime
    - statocles build public
  artifacts:
    paths:
    - public
  only:
    - master
```
