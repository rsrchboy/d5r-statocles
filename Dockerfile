FROM alpine:3.9

ARG IMAGE_TZ=America/Chicago

RUN apk add --no-cache \
        build-base \
        curl \
        git \
        make \
        perl perl-dev \
        perl-app-cpanminus \
        perl-config-any \
        perl-datetime \
        perl-datetime-locale \
        perl-dir-self \
        perl-encode \
        perl-extutils-config \
        perl-extutils-helpers \
        perl-extutils-installpaths \
        perl-import-into \
        perl-ipc-run \
        perl-json \
        perl-list-utilsby \
        perl-lwp-protocol-https \
        perl-module-build \
        perl-module-build-tiny \
        perl-mojolicious \
        perl-moo \
        perl-params-util \
        perl-path-tiny \
        perl-sub-exporter \
        perl-sub-uplevel \
        perl-test-deep \
        perl-test-differences \
        perl-test-exception \
        perl-test-most \
        perl-test-nowarnings \
        perl-try-tiny \
        perl-type-tiny \
        perl-universal-require \
        perl-yaml \
    # for the next cpanm invocation
    && apk add --no-cache \
        perl-carp \
        perl-carp-clan \
        perl-class-load-xs \
        perl-clone \
        perl-cpanel-json-xs \
        perl-date-format \
        perl-devel-stacktrace \
        perl-file-remove \
        perl-json-maybexs \
        perl-log-dispatch \
        perl-module-runtime-conflicts \
        perl-moose \
        perl-package-deprecationmanager \
        perl-test-failwarnings \
        perl-text-template \
        tzdata \
    && ln -snf /usr/share/zoneinfo/$IMAGE_TZ /etc/localtime

# RUN cpanm \
#     Pod::Weaver
# RUN cpanm \
#     PPI
# RUN cpanm \
#     Pod::Elemental
RUN cpanm -q Syntax::Highlight::Engine::Kate \
    && cpanm -q HTML::Lint::Pluggable \
    && cpanm --installdeps -q Statocles \
    # a couple of the tests appear to fail without tty (at least, that's my
    # guess.)  They pass otherwise.
    && cpanm --notest Statocles \
    && rm -rf ~/.cpanm
